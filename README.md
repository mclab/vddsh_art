This repository allows the reviewers to easily inspect data, models and to reproduce the experiments reported in the paper.
The repository comprises:

* Folder `data/` - dataset with the anonymised health records described in the paper
* Folder `modelica/` - the full virtual doctors described in the paper, implemented in [Modelica](https://modelica.org).
* Folder `ptv/` - a Python tool to compute the metrics reported in the paper and to reproduce the experiments therein.

## Software Prerequisites

* [Docker Engine](https://docker.com)
* A Bash shell (either on a Linux/MacOS system, or via [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10)) to run the script called `run`

## Usage


```
PATH_TO_THE_REPO/run RECORD_ID
```

Where:

* `PATH_TO_THE_REPO` is the path to the clone of the current repository.
* `RECORD_ID` is a health record id. Valid ids are filenames in `data` directory without extension.

The results of a tool execution are timing and dosage differences for each visit in the health record.

Example (on a health record encompassing 5 visits):
```
Timing: [0, 0, 0, 2, 0]  in days
Dosage: [0, 0, 0, -2, 0]  in dose quanta
```

Note: on the first run, Docker Engine will download the 
MCLab [JModelica.org](https://jmodelica.org) image
from [Docker Hub](https://hub.docker.com).
After that, internet connection is not anymore required.
Moreover, on the first run of each virtual doctor, the corresponding Modelica model
will be compiled into an [FMU](https://fmi-standard.org) and stored in the `fmu` directory, 
in order to be reused on the subsequent runs. 
This means that the first run of each virtual doctor will be slower than the others.
