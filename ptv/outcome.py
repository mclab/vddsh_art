from collections import OrderedDict
import math

from ptv.util import pairwise

def diff(x, y):
    res = None
    if math.isnan(x):
        if math.isnan(y):
            res = 0
        else:
            res = -float('inf')
    else:
        if math.isnan(y):
            res = float('inf')
        else:
            res = x - y
    return res


class Decision(object):
    def __init__(self, day, dose_shift, horizon):
        self.day = day
        self.dose_shift = dose_shift
        self.horizon = horizon


class Outcome:
    def __init__(self):
        self.decisions = []

    def add(self, day, dose_shift, horizon):
        """Add new decision."""
        self.decisions.append(Decision(day, dose_shift, horizon))

    def timing(self, mc, r_inj):
        """Compute timing mismatches at each decision point."""
        timing = []
        horizon = r_inj.keys()[-1]
        for idx, (decision, decision_next) in enumerate(pairwise(self.decisions + [None])):
            if decision_next is not None:
                r_variation = decision_next.day - decision.day
            else:
                if len(mc.visits) > idx + 1:
                    r_variation = mc.visits[idx + 1].day - decision.day
                else:
                    if len(mc.visits[idx].injections) > 0 and mc.visits[idx].injections[0]['dose'] > 0:
                        r_variation = mc.visits[idx].injections[-1]['day'] - decision.day + 1
                    else:
                        r_variation = float('nan')
            if math.isinf(decision.dose_shift):
                plan_variation = float('nan')
            else:
                plan_variation = decision.horizon - decision.day
            timing.append(diff(r_variation, plan_variation))
        return timing

    def dosage(self, mc, r_inj):
        """Compute timing and dosage errors at each decision point."""
        dosage = []
        horizon = r_inj.keys()[-1]
        for decision in self.decisions:
            plan_variation = decision.dose_shift
            if decision.day <= horizon:
                if (decision.day - 1) in r_inj:
                    r_variation = r_inj[decision.day] - r_inj[decision.day - 1] if r_inj[decision.day] > 0 else float('nan')
                else:
                    r_variation = r_inj[decision.day]
            else:
                r_variation = float('nan')
            dosage.append(diff(r_variation, plan_variation))
            if decision.day > horizon:
                break
        return dosage
