class ValidationError(Exception):
    messages = [
            'Missing age/AFC/AMH measurement',
            'No visits',
            'No injections',
            'Undefined first dose',
            'Preparation failed'
    ]

    def __init__(self, id):
        self.id = id
        self.message = ValidationError.messages[id]


class MissingDataError(ValidationError):
    def __init__(self):
        super(MissingDataError, self).__init__(0)


class NoVisitsError(ValidationError):
    def __init__(self):
        super(NoVisitsError, self).__init__(1)


class NoInjectionsError(ValidationError):
    def __init__(self):
        super(NoInjectionsError, self).__init__(2)


class UndefinedFirstDoseError(ValidationError):
    def __init__(self):
        super(UndefinedFirstDoseError, self).__init__(3)


class PreparationFailedError(ValidationError):
    def __init__(self):
        super(PreparationFailedError, self).__init__(4)
