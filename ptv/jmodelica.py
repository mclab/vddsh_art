import pymodelica
import pyfmi
import copy
import os

from ptv.modelica import Modelica

TEMPLATE = """
model {}
  import Treatments.{}.*;
  import Treatments.*;
  parameter Param p;
  parameter State x = InitialState;
  parameter Request r = InitialRequest;
  parameter Measurement u = Measurement (
    day = 0,
    e2 = 0,
    p4 = 0,
    fp = fill(0, NumFollicleClasses),
    age = 0,
    afc = 0,
    amh = 0,
    prevPrescription = Prescription(
      nextVisit = 0,
      doseGnRHagonist = 0,
      dayStartGnRHagonist = 0,
      dayStopGnRHagonist = 0,
      doseGnRHantagonist = 0,
      dayStartGnRHantagonist = 0,
      dayStopGnRHantagonist = 0,
      doseNorethisterone = 0,
      dayStartNorethisterone = 0,
      dayStopNorethisterone = 0,
      doseStim = 0,
      dayStartStim = 0,
      dayStopStim = 0,
      outcome = Outcome.NONE
    )
  );
  discrete State xnext;
  discrete Prescription y;
  discrete Request rnext;
  parameter Boolean disableAssertions = true;
equation
  (xnext, y, rnext) = Decision(p, x, u, disableAssertions);
end {};
"""


def get_vars(fmu, prefix):
    return {
        name.split('.', 1)[-1]: fmu.get(name)[0]
        for name in fmu.get_model_variables(
            filter='{}.*'.format(prefix)).keys()
    }


def set_vars(fmu, prefix, d):
    for name in d:
        fmu.set('{}.{}'.format(prefix, name), d[name])


class JModelica(Modelica):
    def __init__(self, path_to_lib, path_to_fmu, protocols):
        super(JModelica, self).__init__(path_to_lib, protocols)
        self.dir = path_to_fmu
        self.model_name = 'Treatment'
        for name in self.protocols.values():
            self.__compile(name)

    def decide(self, protocol, p, u, x, y_prev):
        fmu_path = '{}/{}.fmu'.format(self.dir, protocol)
        fmu = pyfmi.load_fmu(
            fmu_path,
            log_file_name='{}/{}_log.txt'.format(self.dir, protocol))
        os.remove('{}/{}_log.txt'.format(self.dir, protocol))
        fmu.set_log_level(0)
        fmu.set('_log_level', 0)
        set_vars(fmu, 'p', p)
        u_ext = copy.copy(u)
        if x is not None:
            set_vars(fmu, 'x', x)
            for v in y_prev:
                u_ext['prevPrescription.{}'.format(v)] = y_prev[v]
        set_vars(fmu, 'u', u_ext)
        try:
            fmu.set('disableAssertions', False)
            fmu.initialize()
            xnext = get_vars(fmu, 'xnext')
            y = get_vars(fmu, 'y')
            r = get_vars(fmu, 'rnext')
            return xnext, y, r
        except pyfmi.fmi.FMUException:
            return None

    def __compile(self, protocol):
        if os.path.isfile('{}/{}.fmu'.format(self.dir, protocol)):
            return
        model_path = '{}/{}.mo'.format(self.dir, protocol)
        with open(model_path, 'w') as model_file:
            spec = TEMPLATE.format(self.model_name, protocol, self.model_name)
            model_file.write(spec)
        pymodelica.compile_fmu(
            class_name=self.model_name,
            file_name=[model_path, self.path_to_lib],
            compile_to='{}/{}.fmu'.format(self.dir, protocol))
        os.remove(model_path)
