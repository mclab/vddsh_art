import math

import ptv.errors as errors
from ptv.outcome import Outcome


def check_preconditions(mc):
    if mc.age is None or mc.afc is None or mc.amh is None:
        raise errors.MissingDataError()
    if len(mc.visits) == 0:
        raise errors.NoVisitsError()
    if len(mc.injections) == 0:
        raise errors.NoInjectionsError()


def prescription_to_doses(now, y):
    start = y['dayStartStim']
    stop = y['dayStopStim']
    horizon = y['nextVisit']
    dose = y['doseStim']
    doses = [0 for i in range(now, start)]
    doses += [dose for i in range(start, stop)]
    doses += [0 for i in range(stop, horizon)]
    return doses


def validate(config, modelica, mc, debug=False):
    check_preconditions(mc)

    p = {}
    for param, value in mc.treatment['params'].items():
        p[param] = value

    u = {'age': mc.age, 'afc': mc.afc, 'amh': mc.amh}
    decision = modelica.decide(mc.treatment['protocol'], p, u, None, None)
    if debug:
        print('====== 0')
        print('u: {}'.format(u))
    if decision is None:
        raise errors.UndefinedFirstDoseError()

    xnext, y, r = decision
    if debug:
        print('y: {}'.format(y))
    x = xnext
    y_prev = y
    complete = True
    outcome = Outcome()
    for i, v in enumerate(mc.visits):
        if debug:
            print('====== {}'.format(i + 1))
        day_decision = v.day if i > 0 else v.day + 1
        if v.e2 is None and not (i == 1 and mc.treatment['protocol'] == 'Short'):
            complete = False
            break
        u = {
            'day': v.day,
            'e2': v.e2
        }  # the first decision is made at stimulation day 0
        if v.p4 is not None:
            u['p4'] = v.p4
        if v.fp is not None:
            for j, f in enumerate(v.fp):
                u['fp[{}]'.format(j + 1)] = f
        elif i > 1:
            if i > 2 or mc.treatment['protocol'] != 'Short':
                complete = False
                break
        if debug:
            print('u: {}'.format(u))
            print('x: {}'.format(x))
            print('y_prev: {}'.format(y_prev))
        decision = modelica.decide(mc.treatment['protocol'], p, u, x, y_prev)
        if decision is None:
            complete = False
            break
        xnext, y, r = decision

        if debug:
            print('y: {}'.format(y))

        if y['outcome'] == 3:
            raise errors.PreparationFailedError()

        x = xnext
        dose_shift = y['doseStim'] - y_prev['doseStim'] if i > 0 else y['doseStim']
        y_prev = y

        if y['outcome'] >= 2:
            dose_shift = float('nan')

        outcome.add(day_decision, dose_shift, y['nextVisit'])
        if len(v.injections) == 0 or y['outcome'] >= 2:
            break
        if config['override_prescription']:
            y_prev['doseStim'] = v.injections[-1]['dose']

    r_inj = mc.injections

    if len(outcome.decisions) > 0:
        timing = outcome.timing(mc, r_inj)
        dosage = outcome.dosage(mc, r_inj)
        return (timing, dosage, complete)
    else:
        return None
