from collections import OrderedDict

from ptv.util import pairwise
from ptv.visit import Visit


class MedicalCase:
    def __init__(self, d):
        self.record = d['record']
        self.age = d['data']['age']
        self.amh = d['data']['amh']
        self.afc = d['data']['afc']
        self.visits = d['visits']
        self.treatment = d['treatment']

    @classmethod
    def from_dict(cls, d):
        dcopy = d.copy()
        dcopy['visits'] = [Visit.from_dict(v) for v in d['visits']]
        return cls(dcopy)

    def to_dict(self):
        d = {}
        d['record'] = self.record
        d['data'] = {}
        d['data']['age'] = self.age
        d['data']['amh'] = self.amh
        d['data']['afc'] = self.afc
        d['visits'] = [v.to_dict() for v in self.visits]
        d['treatment'] = self.treatment
        return d

    @property
    def injections(self):
        res = OrderedDict()
        for v in self.visits:
            for inj in v.injections:
                res[inj['day']] = inj['dose']
        return res

    def __str__(self):
        # type () -> str
        res = ''
        res += 'Record: {}\n'.format(self.record)
        res += 'Protocol: {} {}\n'.format(self.treatment['id'],
                                          self.treatment['params'])
        res += 'Amh: {}\n'.format(self.amh)
        res += 'Afc: {}\n'.format(self.afc)
        res += 'Age: {}\n'.format(self.age)
        for v in self.visits:
            res += 'At day {}'.format(v.day)
            res += ': e2 = {}'.format(v.e2)
            res += ', p4 = {}'.format(v.p4)
            res += ', fp = {}'.format(v.fp)
            res += ', injections = ['
            res += ','.join(['({},{})'.format(inj['day'], inj['dose'])
                             for inj in v.injections])
            res += ']\n'
        return res
