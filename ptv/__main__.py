import sys
import json
from collections import OrderedDict
import pprint

from ptv.medical_case import MedicalCase
from ptv.validate import validate
from ptv.errors import ValidationError
from ptv.jmodelica import JModelica

pp = pprint.PrettyPrinter(indent=4)

def main(config, fname):
    path_to_lib = config['path_to_lib']
    path_to_fmu = config['path_to_fmu']
    modelica = JModelica(path_to_lib, path_to_fmu, config['treatments'])
    path_to_data = config['path_to_data']
    with open('{}/{}'.format(path_to_data, fname)) as f:
        mc_dict = json.load(f, object_pairs_hook=OrderedDict)
        mc = MedicalCase.from_dict(mc_dict)
        try:
            res =  validate(config, modelica, mc)
            if res is not None:
                timing, dosage, _  = res
                print('Timing: {}'.format(timing))
                print('Dosage: {}'.format(dosage))
        except ValidationError as e:
            print(e.message)


with open(sys.argv[1], 'r') as cf:
    config = json.load(cf, object_pairs_hook=OrderedDict)
    fname = '{}.json'.format(sys.argv[2])
    main(config, fname)
