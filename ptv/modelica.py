from abc import ABCMeta, abstractmethod


class Modelica(object):
    __metaclass__ = ABCMeta

    def __init__(self, path_to_lib, protocols):
        self.path_to_lib = path_to_lib
        self.protocols = {int(id): name for id, name in protocols.iteritems()}

    @abstractmethod
    def decide(self, protocol_id, p, u, x, y_prev):
        pass
