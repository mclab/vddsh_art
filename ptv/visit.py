class Visit:
    def __init__(self, day, e2, p4, fp, injections):
        self.day = day
        self.fp = fp
        self.e2 = e2
        self.p4 = p4
        self.injections = injections

    @classmethod
    def from_dict(cls, d):
        return cls(d['day'], d['e2'], d['p4'], d['fp'], d['injections'])

    def to_dict(self):
        return {
            'day': self.day,
            'e2': self.e2,
            'p4': self.p4,
            'fp': self.fp,
            'injections': self.injections
        }
