within Treatments;
package Utils
  function OutcomeIsSuccess
    input Outcome o;
    output Boolean res;
  algorithm
    res := o == Outcome.SUCCESS;
  end OutcomeIsSuccess;

  function OutcomeIsFailure
    input Outcome o;
    output Boolean res;
  algorithm
    res := o <> Outcome.NONE and o <> Outcome.SUCCESS;
  end OutcomeIsFailure;

  function ClassifyValue "Classifies value according to the thresholds."
    input Real value;
    input Real thresholds[:];
    input GenericParam params[:];
    output Integer cl;
  protected
    GenericParam p;
    Real threshold;
  algorithm
    cl := 0;
    for i in 1:size(thresholds, 1) loop
      p := params[i];
      threshold := thresholds[i] + p.k * p.q;
      if value < threshold then
        cl := i;
        break;
      end if;
    end for;
    if cl == 0 then
      cl := size(thresholds, 1) + 1;
    end if;
  end ClassifyValue;

  function SizeToClass "Maps follicle size to follicle class"
    input Real sz;
    output Integer cl;
  algorithm
    cl := Treatments.NumFollicleClasses;
    for i in 1:(Treatments.NumFollicleClasses - 1) loop
      if sz < Treatments.FollicleMinSizes[i + 1] then
        cl := i; // if follicle has size less than min size of class i + 1, then it belongs to class i
        break;
      end if;
    end for;
  end SizeToClass;

  function FolliclesToProfile
    input Follicles.Output follicles[:];
    output FollicleProfile profile;
  protected
    Integer cl;
  algorithm
    profile := fill(0, Treatments.NumFollicleClasses);
    for i in 1:size(follicles, 1) loop
      if follicles[i].isActive and (not follicles[i].isAtretic) then
        cl := SizeToClass(follicles[i].diameter);
        profile[cl] := profile[cl] + 1;
      end if;
    end for;
  end FolliclesToProfile;

  function ProfileCut "Remove extra follicles from follicle profile"
    input FollicleProfile fp;
    input Integer n "Number of follicles to remove";
    output FollicleProfile fpCut;
  protected
    Integer m;
    Integer c;
  algorithm
    fpCut := fp;
    m := n;
    for i in 1:NumFollicleClasses loop
      if m > 0 then // if there are still follicles to be removed
        c := min(fpCut[i], m); // how many we can remove from this class
        fpCut[i] := fpCut[i] - c;
        m := m - c;
      end if;
    end for;
  end ProfileCut;

  function AlignProfiles "Align follicle profiles using the one with less number of follicles."
    input FollicleProfile fp1;
    input FollicleProfile fp2;
    output FollicleProfile fp1Cut;
    output FollicleProfile fp2Cut;
    output Integer n;
  protected
    Integer n1 = sum(fp1);
    Integer n2 = sum(fp2);
    Integer m;
  algorithm
    m := n1 - n2;
    n := if m > 0 then n2 else n1;
    fp1Cut := ProfileCut(fp1, m);
    fp2Cut := ProfileCut(fp2, -m);
  end AlignProfiles;

  function VectorCreateFilled "Create vector of length n, filled with val."
    input Real val;
    input Integer n;
    output Vector v;
  algorithm
    v.count := n;
    v.arr := { if i <= n then val else 0 for i in 1:MaxFolliclesNum };
  end VectorCreateFilled;

  function VectorToProfile "Convert follicles vector to follicles profile"
    input Vector v;
    output FollicleProfile profile;
  protected
    Integer cl;
  algorithm
    profile := fill(0, NumFollicleClasses);
    for i in 1:v.count loop
      cl := SizeToClass(v.arr[i]);
      profile[cl] := profile[cl] + 1;
    end for;
  end VectorToProfile;

  function ProfileToVector "Convert follicles profile to follicles vector"
    input FollicleProfile profile;
    input Real sizes[NumFollicleClasses];
    output Vector follicles;
  algorithm
    follicles.count := 0;
    for i in 1:NumFollicleClasses loop
      for j in 1:profile[i] loop
        follicles.count := follicles.count + 1;
        follicles.arr[follicles.count] := sizes[i];
      end for;
    end for;
  end ProfileToVector;

  function ProfileSatisfiesMSC "Check whether follicles profile satisfies MSC"
    input FollicleProfile profile;
    output Boolean res;
  algorithm
    res := profile[5] + profile[6] + profile[7] >= 3 and (profile[6] + profile[7] >= 2 or profile[7] >= 1);
  end ProfileSatisfiesMSC;

  function VectorSatisfiesMSC "Check whether follicles vector satisfies MSC"
    input Vector v;
    output Boolean res;
  algorithm
    res := ProfileSatisfiesMSC(VectorToProfile(v));
  end VectorSatisfiesMSC;

  function ProfileSatisfiesFSC "Check whether follicles profile satisfies FSC"
    input FollicleProfile profile;
    output Boolean res;
  algorithm
    res := min(15, sum(profile)) <= profile[6] + profile[7];
  end ProfileSatisfiesFSC;

  function VectorSatisfiesFSC "Check whether follicles vector satisfies FSC"
    input Vector v;
    output Boolean res;
  algorithm
    res := ProfileSatisfiesMSC(VectorToProfile(v));
  end VectorSatisfiesFSC;
end Utils;
