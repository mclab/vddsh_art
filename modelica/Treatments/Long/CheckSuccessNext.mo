within Treatments.Long;
function CheckSuccessNext "Compute dose and day of next visit based on 2 TV-US"
  input Integer dayPrev;
  input FollicleProfile fpPrev;
  input Real e2Prev;
  input Integer dayCurr;
  input FollicleProfile fpCurr;
  input Real e2Curr;
  input Integer dose;
  input Integer dayStartStim;
  input Integer durationStim;
  input Integer afc;
  input Integer loopCount;
  output Boolean failE2Risk "if there is no hope of MSC and there is a risk of high E2 with current dose";
  output Boolean failTooLate "if there is no hope of MSC and it is too late to react";
  output Boolean growthWarning;
  output Integer nextVisit;
  output Integer doseNew;
protected
  Integer n;
  FollicleProfile fpPrevAligned;
  Vector vPrevFirst;
  FollicleProfile fpCurrAligned;
  Vector vCurrFirst;
  Vector vCurrLast;
  Vector growthRates;
  SimResult simRes;
algorithm
  failE2Risk := false;
  failTooLate := false;
  (fpPrevAligned, fpCurrAligned, n) := AlignProfiles(fpPrev, fpCurr);
  vPrevFirst := ProfileToVector(fpPrevAligned, FollicleMinSizes);
  vCurrLast := ProfileToVector(fpCurrAligned, FollicleMaxSizes);
  (growthRates, growthWarning) := ComputeGrowthRates(dayPrev, vPrevFirst, dayCurr, vCurrLast, n);
  simRes := Simulate(vCurrLast, growthRates, dayCurr, dayStartStim + durationStim - 1, E2PredictionBeta, E2PredictionGamma, E2RiskThreshold, afc);
  if not simRes.hopeMsc then
    if not simRes.e2Risk then // no MSC and no e2 risk
      if loopCount <= 2 then
        nextVisit := dayCurr + 2;
        doseNew := min(DoseStimMax, dose + 2);
      else
        failTooLate := true;
        nextVisit := dayCurr + 1;
        doseNew := 0;
      end if;
    else // no MSC and e2 risk
      failE2Risk := true;
      nextVisit := dayCurr + 1;
      doseNew := 0;
    end if;
  elseif simRes.hopeFsc then
    if simRes.e2Risk or simRes.manyMatureFolliclesRisk then // FSC and e2 risk
      nextVisit := simRes.dayFsc;
      if min(simRes.dayE2Risk, simRes.dayManyMatureFolliclesRisk) >= simRes.dayFsc then
        doseNew := max(DoseStimMin, dose - 1);
      else
        doseNew := max(DoseStimMin, dose - 2);
      end if;
    else // FSC and no e2 risk
      nextVisit := simRes.dayFsc;
      doseNew := dose;
    end if;
  else // MSC and no FSC
    nextVisit := simRes.dayMsc;
    if simRes.e2Risk then
      if simRes.dayE2Risk >= simRes.dayMsc then // MSC and e2 risk after MSC
        doseNew := max(DoseStimMin, dose - 1);
      else // MSC and e2 risk before MSC
        doseNew := max(DoseStimMin, dose - 2);
      end if;
    else // MSC and no e2 risk
      doseNew := min(DoseStimMax, dose + 2);
    end if;
  end if;
  if not failE2Risk and not failTooLate then
    (nextVisit, doseNew) := CommonSenseRule(dayCurr, dose, nextVisit, doseNew, dayStartStim);
  end if;
end CheckSuccessNext;
