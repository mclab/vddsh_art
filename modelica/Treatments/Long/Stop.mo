within Treatments.Long;
function Stop
  input Param p;
  input State x;
  input Measurement u;
  input Outcome outcome;
  output State xnext;
  output Prescription y;
  output Request r;
algorithm
  (xnext, y, r) := NOP(p, x, u);
  xnext.step := Step.TERMINAL;
  xnext.outcome := outcome;
  y.outcome := outcome;
  y.doseStim := 0;
end Stop;
