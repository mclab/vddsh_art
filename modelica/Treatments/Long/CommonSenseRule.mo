within Treatments.Long;
function CommonSenseRule
  input Integer dayCurr;
  input Integer doseCurr;
  input Integer dayNext;
  input Integer doseNext;
  input Integer dayStartStim;
  output Integer day;
  output Integer dose;
algorithm
  day := dayNext;
  dose := doseNext;
  if day == dayCurr + 1 and dose <> doseCurr then // don't change dose just for 1 day
    dose := doseCurr;
  elseif day > dayCurr + 2 then // next visit at most after 2 days
    day := dayCurr + 2;
    day := max(day, dayStartStim + 10);
  end if;
end CommonSenseRule;
