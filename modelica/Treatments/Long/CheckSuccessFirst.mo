within Treatments.Long;
function CheckSuccessFirst "Compute dose and day of next visit based on 1 TV-US"
  input FollicleProfile profile;
  input Integer dose;
  input Real e2Prev;
  input Real e2;
  input Integer day;
  input Integer dayStartStim;
  input Integer durationStim;
  input Integer afc;
  output Integer dayMsc;
  output Integer doseNew;
protected
  Vector v;
  SimResult res;
algorithm
  v := ProfileToVector(profile, FollicleMaxSizes);
  res := Simulate(v, VectorCreateFilled(GrowthRateStd, v.count), day, dayStartStim + durationStim - 1, E2PredictionBeta, E2PredictionGamma, E2RiskThreshold, afc);
  dayMsc := res.dayMsc;
  doseNew := dose;
  if not res.hopeMsc then
    doseNew := min(doseNew + 2, DoseStimMax);
  end if;
  if (e2 < 1.5 * e2Prev) then doseNew := min(doseNew + 1, DoseStimMax);
  elseif (e2 < 2 * e2Prev) then doseNew := min(doseNew + 2, DoseStimMax);
  elseif (e2 > 3.5 * e2Prev) then doseNew := max(doseNew - 2, DoseStimMin);
  elseif (e2 > 3 * e2Prev) then doseNew := max(doseNew - 1, DoseStimMin);
  end if;
  (dayMsc, doseNew) := CommonSenseRule(day, dose, dayMsc, doseNew, dayStartStim);
end CheckSuccessFirst;
