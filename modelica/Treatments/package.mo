within;
package Treatments
  constant Integer MaxFolliclesNum = 100;
  constant Integer NumFollicleClasses = 8 "Number of follicle classes";
  constant Real FollicleMinSizes[NumFollicleClasses] = {5, 10, 12, 14, 16, 18, 20, 22} "Follicle minimal sizes per class (diameters)";
  constant Real FollicleMaxSizes[NumFollicleClasses] = {9, 11, 13, 15, 17, 19, 21, 40} "Follicle maximal sizes per class (diameters)";
  constant Real DoseStimQuantum = 37.5;
  constant Integer DoseStimMin = 0;
  constant Integer DoseStimMax = 12;
  constant Real AfcThresholds[:] = {5, 8, 16, 21};
  constant Real AgeThresholds[:] = {35, 38, 40};
  constant Real AmhThresholds[:] = {2.5, 7.0, 14.0, 20.0};
  constant RequestPrevPrescription PrevPrescriptionIsNeeded = RequestPrevPrescription(
    nextVisit = true,
    doseDecapeptyl = true,
    dayStartDecapeptyl = true,
    dayStopDecapeptyl = true,
    doseNorethisterone = true,
    dayStartNorethisterone = true,
    dayStopNorethisterone = true,
    doseStim = true,
    dayStartStim = true,
    dayStopStim = true,
    outcome = true
  );
  constant RequestPrevPrescription PrevPrescriptionIsNotNeeded = RequestPrevPrescription(
    nextVisit = false,
    doseDecapeptyl = false,
    dayStartDecapeptyl = false,
    dayStopDecapeptyl = false,
    doseNorethisterone = false,
    dayStartNorethisterone = false,
    dayStopNorethisterone = false,
    doseStim = false,
    dayStartStim = false,
    dayStopStim = false,
    outcome = false
  );
  type FollicleProfile = Integer[NumFollicleClasses];
  record Vector
    Real arr[MaxFolliclesNum];
    Integer count;
  end Vector;
  connector MeasurementInput = input Measurement;
  connector MeasurementOutput = output Measurement;
  connector PrescriptionInput = input Prescription;
  connector PrescriptionOutput = output Prescription;
end Treatments;
