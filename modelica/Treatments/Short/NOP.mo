within Treatments.Short;
function NOP
  input Param p;
  input State x;
  input Measurement u;
  output State xnext;
  output Prescription y;
  output Request r;
algorithm
  // state doesn't change
  xnext := x;
  // 0 doses, come tomorrow
  y.nextVisit := u.day + 2;
  y.doseGnRHagonist := 0;
  y.dayStartGnRHagonist := u.day;
  y.dayStopGnRHagonist := u.day;
  y.doseGnRHantagonist := 0;
  y.dayStartGnRHantagonist := u.day;
  y.dayStopGnRHantagonist := u.day;
  y.doseNorethisterone := 0;
  y.dayStartNorethisterone := u.day;
  y.dayStopNorethisterone := u.day;
  y.doseStim := u.prevPrescription.doseStim;
  y.dayStartStim := u.day;
  y.dayStopStim := u.day + 1;
  y.outcome := x.outcome;
  // no measurements are needed, just current day
  r.day := true;
  r.e2 := false;
  r.p4 := false;
  r.fp := fill(false, NumFollicleClasses);
  r.age := false;
  r.afc := false;
  r.amh := false;
end NOP;
