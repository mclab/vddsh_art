within Treatments.Short;
record Param
  parameter Integer dayStartPrep = 2;
  parameter Integer durationPrep = 14;
  parameter Integer daysBeforeFirstCheck = 2;
  parameter Integer durationStim = 13;
  parameter Boolean useRuleDelayStim = false;

  parameter GenericParam amhThreshold = GenericParam(0, 3.5);
  parameter GenericParam afcThreshold = GenericParam(0, 3);
  parameter GenericParam ageThreshold = GenericParam(0, 2);
  parameter GenericParam e2Threshold = GenericParam(0, 300);
  parameter GenericParam doseShift = GenericParam(0, 1);

  parameter GenericParam amhThresholds[:] = { amhThreshold for i in 1:size(AmhThresholds, 1) };
  parameter GenericParam afcThresholds[:] = { afcThreshold for i in 1:size(AfcThresholds, 1) };
  parameter GenericParam ageThresholds[:] = { ageThreshold for i in 1:size(AgeThresholds, 1) };
  parameter GenericParam e2Thresholds[:] = { e2Threshold for i in 1:size(E2Thresholds, 1) };
end Param;
