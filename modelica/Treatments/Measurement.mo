within Treatments;
record Measurement "Treatment input"
  Integer day "Current day";
  Real e2 "Estradiol level";
  Real p4 "Progesterone level";
  Integer fp[NumFollicleClasses] "Follicle profile";
  Integer age "Patient age";
  Integer afc "Antral follicle count";
  Real amh "Anti-Mullerian Hormone level";
  Prescription prevPrescription "Previous prescription";
end Measurement;
