within Treatments;
package Anta
  import Treatments.Outcome;
  import Treatments.Measurement;
  import Treatments.Prescription;
  import Treatments.Request;
  import Treatments.RequestPrevPrescription;
  import Treatments.NumFollicleClasses;
  import Treatments.FollicleMinSizes;
  import Treatments.FollicleMaxSizes;
  import Treatments.AfcThresholds;
  import Treatments.AgeThresholds;
  import Treatments.AmhThresholds;
  import Treatments.GenericParam;
  import Treatments.Utils.ClassifyValue;
  import Treatments.Utils.SizeToClass;
  import Treatments.FollicleProfile;
  import Treatments.Vector;
  import Treatments.Utils.VectorCreateFilled;
  import Treatments.Utils.VectorToProfile;
  import Treatments.Utils.ProfileToVector;
  import Treatments.Utils.AlignProfiles;
  import Treatments.Utils.VectorSatisfiesMSC;
  import Treatments.Utils.ProfileSatisfiesMSC;
  import Treatments.Utils.VectorSatisfiesFSC;
  import Treatments.Utils.ProfileSatisfiesFSC;
  import Treatments.VPH.SimResult;
  import Treatments.VPH.Simulate;
  import Treatments.VPH.ComputeGrowthRates;
  import Treatments.VPH.PredictE2;
  import Treatments.DoseStimMin;
  import Treatments.DoseStimMax;

  type Step = enumeration (PLANNING, FIRST_CHECK, MID_CHECK, FINAL_LOOP, NEXT_LOOP, TERMINAL);

  constant State InitialState = State (
    needP4 = false,
    e2 = 0,
    day = 0,
    step = Step.PLANNING,
    ageClass = 0,
    amhClass = 0,
    afcClass = 0,
    afc = 0,
    fp = fill(0, NumFollicleClasses),
    loopCount = 0,
    dayStartStim = 0,
    outcome = Outcome.NONE
  );

  constant Request InitialRequest = Request (
    day = false,
    e2 = false,
    p4 = false,
    fp = fill(false, NumFollicleClasses),
    age = true,
    afc = true,
    amh = true,
    prevPrescription = PrevPrescriptionIsNotNeeded
  );

  constant Real DoseGnRHantagonist = 0.1;
  constant Real DoseNorethisterone = 10;
  constant Integer DaysBeforeMidCheck = 6;
  constant Integer DaysAfterMidCheck = 2;
  constant Integer MaxVisitDistance = 3;
  constant Real E2SafetyThresholdFirst = 300;
  constant Real E2RiskThreshold = 24000; // IVANO: check threshold 20000 or 24000 ????
  constant Real P4MeasurementThreshold = 4;
  constant Real P4SafetyThresholdFirst = 6;
  constant Real P4SafetyThreshold = 40;
  constant Integer AfcSafetyThreshold = 3;
  constant Real GrowthRateStd = 1.6;
  constant Real E2PredictionBeta = 0.795;
  constant Real E2PredictionGamma = 1.4;

  constant Real E2Thresholds[:] = {500, 850, 3000};

  constant Integer FirstDoses[5, 5, 4] =
  {{{0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 0, 0, 0}},

   {{0, 0, 0, 0},
    {0, 0, 0, 0},
    {4, 4, 4, 6},
    {4, 4, 4, 6},
    {3, 3, 3, 4}},

   {{0, 0, 0, 0},
    {0, 0, 0, 0},
    {4, 4, 4, 6},
    {4, 4, 4, 6},
    {3, 3, 3, 4}},

   {{0, 0, 0, 0},
    {0, 0, 0, 0},
    {3, 3, 3, 4},
    {3, 3, 3, 4},
    {2, 2, 2, 3}},

   {{0, 0, 0, 0},
    {0, 0, 0, 0},
    {3, 3, 3, 4},
    {3, 3, 3, 4},
    {2, 2, 2, 3}}};

  constant Integer SecondDoses[4, 5, 5, 4] =

    //  E2 < 500

  {{{{0, 0, 0, 0},
     {0, 0, 0, 0},
     {0, 0, 0, 0},
     {0, 0, 0, 0},
     {0, 0, 0, 0}},

    {{0, 0, 0, 0},
     {0, 0, 0, 0},
     {6, 6, 6, 8},
     {6, 6, 6, 8},
     {4, 4, 4, 5}},

    {{0, 0, 0, 0},
     {0, 0, 0, 0},
     {6, 6, 6, 8},
     {6, 6, 6, 8},
     {4, 4, 4, 5}},

    {{0, 0, 0, 0},
     {0, 0, 0, 0},
     {4, 4, 4, 5},
     {4, 4, 4, 5},
     {3, 3, 3, 4}},

    {{0, 0, 0, 0},
     {0, 0, 0, 0},
     {4, 4, 4, 5},
     {4, 4, 4, 5},
     {3, 3, 3, 4}}},

     // 500<=E2<850

   {{{0, 0, 0, 0},
     {0, 0, 0, 0},
     {0, 0, 0, 0},
     {0, 0, 0, 0},
     {0, 0, 0, 0}},

    {{0, 0, 0, 0},
     {0, 0, 0, 0},
     {4, 4, 4, 6},
     {4, 4, 4, 6},
     {3, 3, 3, 4}},

    {{0, 0, 0, 0},
     {0, 0, 0, 0},
     {4, 4, 4, 6},
     {4, 4, 4, 6},
     {3, 3, 3, 4}},

    {{0, 0, 0, 0},
     {0, 0, 0, 0},
     {3, 3, 3, 4},
     {3, 3, 3, 4},
     {2, 2, 2, 3}},

    {{0, 0, 0, 0},
     {0, 0, 0, 0},
     {3, 3, 3, 4},
     {3, 3, 3, 4},
     {2, 2, 2, 3}}},

     // 850 <= E2 < 3000
    {{{0, 0, 0, 0},
      {0, 0, 0, 0},
      {0, 0, 0, 0},
      {0, 0, 0, 0},
      {0, 0, 0, 0}},

     {{0, 0, 0, 0},
      {0, 0, 0, 0},
      {4, 4, 4, 6},
      {4, 4, 4, 6},
      {3, 3, 3, 4}},

     {{0, 0, 0, 0},
      {0, 0, 0, 0},
      {4, 4, 4, 6},
      {4, 4, 4, 6},
      {3, 3, 3, 4}},

     {{0, 0, 0, 0},
      {0, 0, 0, 0},
      {3, 3, 3, 4},
      {3, 3, 3, 4},
      {2, 2, 2, 3}},

     {{0, 0, 0, 0},
      {0, 0, 0, 0},
      {3, 3, 3, 4},
      {3, 3, 3, 4},
      {2, 2, 2, 3}}},

     // E2 > 3000

   {{{0, 0, 0, 0},
     {0, 0, 0, 0},
     {0, 0, 0, 0},
     {0, 0, 0, 0},
     {0, 0, 0, 0}},

    {{0, 0, 0, 0},
     {0, 0, 0, 0},
     {4, 4, 4, 6},
     {4, 4, 4, 6},
     {2, 2, 2, 4}},

    {{0, 0, 0, 0},
     {0, 0, 0, 0},
     {4, 4, 4, 6},
     {4, 4, 4, 6},
     {2, 2, 2, 4}},

    {{0, 0, 0, 0},
     {0, 0, 0, 0},
     {3, 3, 3, 7},
     {3, 3, 3, 7},
     {3, 3, 3, 7}},

    {{0, 0, 0, 0},
     {0, 0, 0, 0},
     {3, 3, 3, 7},
     {3, 3, 3, 7},
     {3, 3, 3, 7}}}};
end Anta;
