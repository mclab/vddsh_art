within Treatments.Anta;
function Decision
  input Param p;
  input State x;
  input Treatments.Measurement u;
  input Boolean disableAssert;
  output State xnext;
  output Treatments.Prescription y;
  output Treatments.Request r;
protected
  Boolean growthWarning;
  Boolean e2Risk;
  Boolean tooLate;
  Integer expectation;
  Integer d;
  Integer amhClass;
  Integer afcClass;
  Integer ageClass;
algorithm
  if x.step == Step.PLANNING then
    amhClass := ClassifyValue(u.amh, AmhThresholds, p.amhThresholds);
    afcClass := ClassifyValue(u.afc, AfcThresholds, p.afcThresholds);
    ageClass :=  ClassifyValue(u.age, AgeThresholds, p.ageThresholds);
    y.nextVisit := p.dayStartPrep + p.durationPrep - 1 + p.daysBeforeFirstCheck;
    y.doseGnRHagonist := 0;
    y.dayStartGnRHagonist := u.day;
    y.dayStopGnRHagonist := u.day;
    y.doseGnRHantagonist := 0;
    y.dayStartGnRHantagonist := u.day;
    y.dayStopGnRHantagonist := u.day;
    y.doseNorethisterone := DoseNorethisterone;
    y.dayStartNorethisterone := p.dayStartPrep;
    y.dayStopNorethisterone := p.dayStartPrep + p.durationPrep - 1;
    d := FirstDoses[amhClass, afcClass, ageClass];
    assert(disableAssert or d <> 0, "first dose is 0");
    d := d + 1;
    y.doseStim := max(min(d + integer(p.doseShift.k * p.doseShift.q), DoseStimMax), DoseStimMin);
    y.dayStartStim := y.nextVisit + 1;
    y.dayStopStim := y.nextVisit + DaysBeforeMidCheck;
    y.outcome := Outcome.NONE;

    xnext := x;
    xnext.step := Step.FIRST_CHECK;
    xnext.amhClass := amhClass;
    xnext.afcClass := afcClass;
    xnext.ageClass := ageClass;
    xnext.dayStartStim := y.dayStartStim;
    xnext.outcome := y.outcome;

    r.day := true;
    r.e2 := true;
    r.p4 := true;
    r.fp := fill(true, NumFollicleClasses);
    r.age := false;
    r.afc := false;
    r.amh := false;
    r.prevPrescription := PrevPrescriptionIsNeeded;

  elseif x.step == Step.FIRST_CHECK then
    if u.e2 >= E2SafetyThresholdFirst or u.p4 >= P4SafetyThresholdFirst then
      (xnext, y, r) := Stop(p, x, u, Outcome.FAILURE_DOWNREGULATION);
    else
      y.nextVisit := u.prevPrescription.dayStopStim;
      y.doseGnRHagonist := 0;
      y.dayStartGnRHagonist := u.day;
      y.dayStopGnRHagonist := u.day;
      y.doseGnRHantagonist := 0;
      y.dayStartGnRHantagonist := u.day;
      y.dayStopGnRHantagonist := u.day;
      y.doseNorethisterone := 0;
      y.dayStartNorethisterone := u.day;
      y.dayStopNorethisterone := u.day;
      y.doseStim := u.prevPrescription.doseStim;
      y.dayStartStim := u.prevPrescription.dayStartStim;
      y.dayStopStim := u.prevPrescription.dayStopStim;
      y.outcome := Outcome.NONE;

      xnext := x;
      xnext.step := Step.MID_CHECK;
      xnext.e2 := u.e2;
      xnext.day := u.day;
      xnext.afc := sum(u.fp);
      xnext.fp := u.fp;
      xnext.dayStartStim := y.dayStartStim;
      xnext.outcome := y.outcome;

      r.day := true;
      r.e2 := true;
      r.p4 := true;
      r.fp := fill(false, NumFollicleClasses);
      r.age := false;
      r.afc := false;
      r.amh := false;
      r.prevPrescription := PrevPrescriptionIsNeeded;
    end if;

  elseif x.step == Step.MID_CHECK then
    if u.p4 >= P4SafetyThreshold then
      (xnext, y, r) := Stop(p, x, u, Outcome.FAILURE_P4_STIMULATION);
    else
      y.nextVisit := u.day + DaysAfterMidCheck;
      y.doseGnRHagonist := 0;
      y.dayStartGnRHagonist := u.day;
      y.dayStopGnRHagonist := u.day;
      y.doseGnRHantagonist := DoseGnRHantagonist;
      y.dayStartGnRHantagonist := u.day;
      y.dayStopGnRHantagonist := y.nextVisit;
      y.doseNorethisterone := 0;
      y.dayStartNorethisterone := u.day;
      y.dayStopNorethisterone := u.day;
      d := SecondDoses[ClassifyValue(u.e2, E2Thresholds, p.e2Thresholds), x.amhClass, x.afcClass, x.ageClass];
      assert(disableAssert or d <> 0, "second dose is 0");
      d := d + 1;
      y.doseStim := max(min(d + integer(p.doseShift.k * p.doseShift.q), DoseStimMax), DoseStimMin);
      y.dayStartStim := u.day;
      y.dayStopStim := y.nextVisit;
      y.outcome := Outcome.NONE;

      xnext := x;
      xnext.step := Step.FINAL_LOOP;
      xnext.needP4 := (u.p4 >= P4MeasurementThreshold);
      xnext.e2 := u.e2;
      xnext.day := u.day;
      xnext.outcome := y.outcome;

      r.day := true;
      r.e2 := true;
      r.p4 := xnext.needP4;
      r.fp := fill(true, NumFollicleClasses);
      r.age := false;
      r.afc := false;
      r.amh := false;
      r.prevPrescription := PrevPrescriptionIsNeeded;
    end if;

  elseif x.step == Step.FINAL_LOOP then
    if x.needP4 and u.p4 >= P4SafetyThreshold then
      (xnext, y, r) := Stop(p, x, u, Outcome.FAILURE_P4_STIMULATION);
    elseif sum(u.fp) < AfcSafetyThreshold then
      (xnext, y, r) := Stop(p, x, u, Outcome.FAILURE_AFC_LOW);
    elseif u.day > x.dayStartStim + p.durationStim - 1 then
      (xnext, y, r) := Stop(p, x, u, Outcome.FAILURE_MSC_NOT_ACHIEVED);
    elseif ProfileSatisfiesMSC(u.fp) then
      (xnext, y, r) := Stop(p, x, u, Outcome.SUCCESS);
    else
      (y.nextVisit, d) := CheckSuccessFirst(u.fp, u.prevPrescription.doseStim, x.e2, u.e2, u.day, x.dayStartStim, p.durationStim, x.afc);
      y.doseGnRHagonist := 0;
      y.dayStartGnRHagonist := u.day;
      y.dayStopGnRHagonist := u.day;
      y.doseGnRHantagonist := DoseGnRHantagonist;
      y.dayStartGnRHantagonist := u.day;
      y.dayStopGnRHantagonist := y.nextVisit;
      y.doseNorethisterone := 0;
      y.dayStartNorethisterone := u.day;
      y.dayStopNorethisterone := u.day;
      y.doseStim := d;
      y.dayStartStim := u.day;
      y.dayStopStim := y.nextVisit;
      y.outcome := Outcome.NONE;

      xnext := x;
      xnext.step := Step.NEXT_LOOP;
      xnext.e2 := u.e2;
      xnext.day := u.day;
      xnext.fp := u.fp;
      xnext.loopCount := 1;
      xnext.outcome := y.outcome;

      r.day := true;
      r.e2 := true;
      r.p4 := x.needP4;
      r.fp := fill(true, NumFollicleClasses);
      r.age := false;
      r.afc := false;
      r.amh := false;
      r.prevPrescription := PrevPrescriptionIsNeeded;
    end if;

  elseif x.step == Step.NEXT_LOOP then
    if x.needP4 and u.p4 >= P4SafetyThreshold then
      (xnext, y, r) := Stop(p, x, u, Outcome.FAILURE_P4_STIMULATION);
    elseif sum(u.fp) < AfcSafetyThreshold then
      (xnext, y, r) := Stop(p, x, u, Outcome.FAILURE_AFC_LOW);
    elseif u.day > x.dayStartStim + p.durationStim - 1 then
      (xnext, y, r) := Stop(p, x, u, Outcome.FAILURE_MSC_NOT_ACHIEVED);
    elseif ProfileSatisfiesMSC(u.fp) then
      (xnext, y, r) := Stop(p, x, u, Outcome.SUCCESS);
    else
      (e2Risk, tooLate, growthWarning, y.nextVisit, d) := CheckSuccessNext(x.day, x.fp, x.e2, u.day, u.fp, u.e2, u.prevPrescription.doseStim, x.dayStartStim, p.durationStim, x.afc, x.loopCount);
      if e2Risk then
        (xnext, y, r) := Stop(p, x, u, Outcome.FAILURE_E2_RISK);
      elseif tooLate then
        (xnext, y, r) := Stop(p, x, u, Outcome.FAILURE_TOO_LATE);
      else
        y.doseGnRHagonist := 0;
        y.dayStartGnRHagonist := u.day;
        y.dayStopGnRHagonist := u.day;
        y.doseGnRHantagonist := DoseGnRHantagonist;
        y.dayStartGnRHantagonist := u.day;
        y.dayStopGnRHantagonist := y.nextVisit;
        y.doseNorethisterone := 0;
        y.dayStartNorethisterone := u.day;
        y.dayStopNorethisterone := u.day;
        y.doseStim := d;
        y.dayStartStim := u.day;
        y.dayStopStim := y.nextVisit;
        y.outcome := Outcome.NONE;

        xnext := x;
        xnext.step := Step.NEXT_LOOP;
        xnext.e2 := u.e2;
        xnext.day := u.day;
        xnext.fp := u.fp;
        xnext.loopCount := x.loopCount + 1;
        xnext.outcome := y.outcome;

        r.day := true;
        r.e2 := true;
        r.p4 := x.needP4;
        r.fp := fill(true, NumFollicleClasses);
        r.age := false;
        r.afc := false;
        r.amh := false;
        r.prevPrescription := PrevPrescriptionIsNeeded;
      end if;
    end if;

  elseif x.step == Step.TERMINAL then
    (xnext, y, r) := NOP(p, x, u);
  end if;
end Decision;
