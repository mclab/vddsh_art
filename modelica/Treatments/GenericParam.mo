within Treatments;
record GenericParam
  Integer k;
  Real q;
end GenericParam;
