within Treatments;
block Administration
  PrescriptionInput p;
  GynCycle.Administrations.InjectionsOutput GnRHagonist "Injections of GnRH agonist";
  GynCycle.Administrations.InjectionsOutput Norethisterone "Injections of Norethisterone";
  GynCycle.Administrations.InjectionsOutput Stim "Injections of stimulation drug";
  parameter Real doseStimMultiplier = 1;
algorithm
  when sample(0, 1) then
    if time >= pre(p.nextVisit) then
      GnRHagonist.num := pre(GnRHagonist.num) + (if p.dayStartGnRHagonist < p.nextVisit then min(p.dayStopGnRHagonist, p.nextVisit) - p.dayStartGnRHagonist else 0);
      Norethisterone.num := pre(Norethisterone.num) + (if p.dayStartNorethisterone < p.nextVisit then 2 * (min(p.dayStopNorethisterone, p.nextVisit) - p.dayStartNorethisterone) else 0);
      Stim.num := pre(Stim.num) + (if p.dayStartStim < p.nextVisit then min(p.dayStopStim, p.nextVisit) - p.dayStartStim else 0);
      for i in 1:GynCycle.Administrations.MaxInjectionsNum loop
        if i >= pre(GnRHagonist.num) + 1 and i <= GnRHagonist.num then
          GnRHagonist.arr[i].timing :=  p.dayStartGnRHagonist + i - pre(GnRHagonist.num) - 1;
          GnRHagonist.arr[i].dose := p.doseGnRHagonist;
        end if;
        if i >= pre(Norethisterone.num) + 1 and i <= Norethisterone.num then
          Norethisterone.arr[i].timing :=  p.dayStartNorethisterone + div(i - pre(Norethisterone.num) - 1, 2) + rem(i - pre(Norethisterone.num) - 1, 2) * 0.5 + 0.25;
          Norethisterone.arr[i].dose := p.doseNorethisterone / 2;
        end if;
        if i >= pre(Stim.num) + 1 and i <= Stim.num then
          Stim.arr[i].timing :=  p.dayStartStim + i - pre(Stim.num) - 1;
          Stim.arr[i].dose := p.doseStim * DoseStimQuantum * doseStimMultiplier;
        end if;
      end for;
    end if;
  end when;
end Administration;
