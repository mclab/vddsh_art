within Treatments;
package VPH
  import Treatments.NumFollicleClasses;
  import Treatments.FollicleMinSizes;
  import Treatments.FollicleMaxSizes;
  import Treatments.Vector;
  import Treatments.Utils.SizeToClass;
  import Treatments.Utils.VectorSatisfiesMSC;
  import Treatments.Utils.VectorSatisfiesFSC;
  import Treatments.Utils.VectorToProfile;
  constant Real E2MinFollicleSize = 10;
  constant Real E2MaxFollicleSize = 40;
  constant Real GrowthRateMin = 0;
  constant Real GrowthRateMax = 3.2;
end VPH;
