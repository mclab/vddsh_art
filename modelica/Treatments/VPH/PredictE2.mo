within Treatments.VPH;
function PredictE2 "Given follicles sizes predicts value of E2"
  import Modelica.Constants.pi;
  input Vector v;
  input Real beta;
  input Real gamma;
  output Real e2;
protected
  Real surface;
algorithm
  surface := 0;
  for i in 1:v.count loop
    if v.arr[i] >= E2MinFollicleSize and v.arr[i] <= E2MaxFollicleSize then
      surface := surface + v.arr[i] * v.arr[i];
    end if;
  end for;
  surface := pi * surface;
  e2 := beta * surface * gamma;
end PredictE2;
