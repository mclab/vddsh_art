within Treatments.VPH;
function Simulate
  input Vector v "Follicle sizes";
  input Vector g "Follicle growth rates";
  input Integer d1;
  input Integer d2;
  input Real e2PredBeta;
  input Real e2PredGamma;
  input Real e2RiskThreshold;
  input Integer afc;
  output SimResult res;
protected
  Vector vCopy = v;
  FollicleProfile fp;
  Real e2Predicted;
algorithm
  res.hopeMsc := false;
  res.dayMsc := d2 + 1;
  res.hopeFsc := false;
  res.dayFsc := d2 + 1;
  res.e2Risk := false;
  res.dayE2Risk := d2 + 1;
  res.manyMatureFolliclesRisk := false;
  res.dayManyMatureFolliclesRisk := d2 + 1;
  for d in (d1 + 1):d2 loop
    for i in 1:vCopy.count loop
      vCopy.arr[i] := vCopy.arr[i] + g.arr[i];
    end for;
    fp := VectorToProfile(vCopy);
    if not res.hopeMsc and VectorSatisfiesMSC(vCopy) then
      res.hopeMsc := true;
      res.dayMsc := d;
    end if;
    if not res.hopeFsc and VectorSatisfiesFSC(vCopy) then
      res.hopeFsc := true;
      res.dayFsc := d;
    end if;
    e2Predicted := PredictE2(vCopy, e2PredBeta, e2PredGamma);
    if not res.e2Risk and e2Predicted > e2RiskThreshold then
      res.e2Risk := true;
      res.dayE2Risk := d;
    end if;
    if not res.manyMatureFolliclesRisk and (fp[6] + fp[7] > min(afc, 15) + 5) then
      res.manyMatureFolliclesRisk := true;
      res.dayManyMatureFolliclesRisk := d;
    end if;
  end for;
end Simulate;
