within Treatments.VPH;
function ComputeGrowthRates "Compute growth rates"
  input Integer dayPrev;
  input Vector vPrev;
  input Integer dayCurr;
  input Vector vCurr;
  input Integer n;
  output Vector g "growth rates for each follicle";
  output Boolean warning "true if some for some follicle it was not possible to find admissible growth rate";
protected
  Boolean adm;
  Vector vPrevCopy = vPrev;
  Vector vCurrCopy = vCurr;
  // workaround for openmodelica
  Real szMin[NumFollicleClasses] = FollicleMinSizes;
  Real szMax[NumFollicleClasses] = FollicleMaxSizes;
algorithm
  warning := false;
  g.count := n;
  for i in 1:n loop
    adm := false;
    while not adm loop
      g.arr[i] := (vCurrCopy.arr[i] - vPrevCopy.arr[i]) / (dayCurr - dayPrev); // compute growth rate
      if g.arr[i] > GrowthRateMax then
        // if it is bigger than maximum
        adm := false;
        if vCurrCopy.arr[i] > szMin[SizeToClass(vCurrCopy.arr[i])] then // try to decrease the current size
          vCurrCopy.arr[i] := vCurrCopy.arr[i] - 1;
        elseif vPrevCopy.arr[i] < szMax[SizeToClass(vPrevCopy.arr[i])] then // try to increase the previous size
          vPrevCopy.arr[i] := vPrevCopy.arr[i] + 1;
        else
          // not possible to adjust sizes
          g.arr[i] := GrowthRateMax;
          adm := true;
          warning := true;
        end if;
      elseif g.arr[i] < GrowthRateMin then
        // if it is bigger than minimum
        adm := false;
        if vCurrCopy.arr[i] < szMax[SizeToClass(vCurrCopy.arr[i])] then // try to increase the current size
          vCurrCopy.arr[i] := vCurrCopy.arr[i] + 1;
        elseif vPrevCopy.arr[i] > szMin[SizeToClass(vPrevCopy.arr[i])] then // try to decrease the previous size
          vPrevCopy.arr[i] := vPrevCopy.arr[i] - 1;
        else
          // not possible to adjust sizes
          g.arr[i] := GrowthRateMin;
          adm := true;
          warning := true;
        end if;
      else
        adm := true;
      end if;
    end while;
  end for;
end ComputeGrowthRates;
